package de.vie.images;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class CopyrightTool {

  private static final Logger logger = Logger.getLogger(CopyrightTool.class.getName());

  private static String inputDirectoryPath;

  private static String outputDirectoryPath;

  private static final String copyright =
      "\r\nSämtliche Texte, Bilder und andere veröffentlichten Informationen unterliegen -sofern nicht anders gekennzeichnet- dem Copyright der Verbände im Einsatz GmbH & Co. Vertriebs KG oder werden mit Erlaubnis der Rechteinhaber veröffentlicht. Jede Verlinkung, Vervielfältigung, Verbreitung, Sendung und Wieder- bzw. Weitergabe der Inhalte ist ohne schriftliche Genehmigung der Verbände im Einsatz GmbH & Co. Vertriebs KG ausdrücklich untersagt.\r\n";

  public static void main(String[] args) throws Exception {

    readInputParameter(args);

    if (inputDirectoryPath != null && outputDirectoryPath != null) {

      logger.log(Level.INFO, "Start processing input directory " + inputDirectoryPath
          + " to output directory " + outputDirectoryPath);

      process();
    } else {
      logger.info("Please set the input and output directory with parameter -id and -od");
    }
  }

  private static void readInputParameter(String[] args) {
    for (int i = 0; i < args.length; i++) {

      switch (args[i]) {
        case "-id": {
          inputDirectoryPath = args[i + 1];
          break;
        }
        case "-od": {
          outputDirectoryPath = args[i + 1];
          break;
        }
      }

    }
  }

  private static void process() {

    try {

      Files.walk(Paths.get(inputDirectoryPath), Integer.MAX_VALUE).filter(Files::isDirectory).forEach(dir -> {

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(dir)) {

          logger.info("Processing directory " + dir.getFileName());

          directoryStream.forEach(path -> {

            if (Files.isRegularFile(path) && path.getFileName().toString().contains("svg")) {

              logger.log(Level.INFO, "Processing file " + path.getFileName());
              
              DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
              dbf.setValidating(false);

              DocumentBuilder db;
              try {

                db = dbf.newDocumentBuilder();

                Document doc = db.parse(new ByteArrayInputStream(Files.readAllBytes(path)));

                Element element = doc.getDocumentElement();
                Comment comment = doc.createComment(copyright);
                element.getParentNode().insertBefore(comment, element);

                if (!Files.exists(Paths.get(outputDirectoryPath
                    + FileSystems.getDefault().getSeparator() + dir.getFileName()))) {
                  Files.createDirectory(Paths.get(outputDirectoryPath
                      + FileSystems.getDefault().getSeparator() + dir.getFileName()));
                }

                write(doc,
                    Paths.get(outputDirectoryPath + FileSystems.getDefault().getSeparator()
                        + dir.getFileName() + FileSystems.getDefault().getSeparator()
                        + replaceNotGoodCharactersInFileName(path.getFileName().toString())));

              } catch (ParserConfigurationException e) {
                logger.log(Level.SEVERE, "Error during parsing file " + path, e);
              } catch (SAXException e) {
                logger.log(Level.SEVERE, "Error during parsing file " + path, e);
              } catch (Exception e) {
                logger.log(Level.SEVERE, "Error during writing file " + path, e);
              }

            }

          });

        } catch (IOException ex) {
          logger.log(Level.INFO, "Error during search file in data dir", ex);
        }


      });

    } catch (IOException e1) {
      logger.log(Level.INFO, "Error during parsing input directory file in data dir", e1);
    }
  }

  private static final void write(Document xml, Path outputFile) throws Exception {

    Transformer tf = TransformerFactory.newInstance().newTransformer();
    tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    tf.setOutputProperty(OutputKeys.INDENT, "yes");
    DocumentType doctype = xml.getDoctype();
    if (doctype != null) {
      tf.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
      tf.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
    }

    logger.info("Write file "+outputFile.getFileName());
    Writer out = new FileWriter(outputFile.toFile());
    tf.transform(new DOMSource(xml), new StreamResult(out));
    out.flush();
    out.close();
  }

  private static String replaceNotGoodCharactersInFileName(String filename) {
    
    return filename.replaceAll("\\s", "_").replaceAll("Ü", "Ue").replaceAll("Ä", "Ae").replaceAll("Ö", "Oe").replaceAll("ü","ue")
        .replaceAll("ö", "oe").replaceAll("ä", "ae").replace("ß", "ss");
    
  }
  
}